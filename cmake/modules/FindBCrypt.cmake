set(BCRYPT_ROOT "${BCRYPT_ROOT}" CACHE PATH "Root directory to look for libbcrypt")

find_library(
	BCRYPT_LIBRARY 
	NAMES 
		libbcrypt
	PATHS
		"${BCRYPT_ROOT}"
		"$ENV{ProgramFiles}/bcrypt"
		"$ENV{ProgramW6432}/bcrypt"
		"$ENV{ProgramFiles}/libbcrypt"
		"$ENV{ProgramW6432}/libbcrypt"
		"/usr/local/lib"
	PATH_SUFFIXES 
		lib
)

find_path(
	BCRYPT_INCLUDE_DIR 
	NAMES 
		bcrypt/bcrypt.h
	PATHS 
		"${BCRYPT_ROOT}" 
		"$ENV{ProgramFiles}/bcrypt"
		"$ENV{ProgramW6432}/bcrypt"
		"$ENV{ProgramFiles}/libbcrypt"
		"$ENV{ProgramW6432}/libbcrypt"
		"/usr/local/include"
	PATH_SUFFIXES 
		include
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(BCrypt
	DEFAULT_MSG
	BCRYPT_LIBRARY
	BCRYPT_INCLUDE_DIR
)

mark_as_advanced(BCRYPT_LIBRARY BCRYPT_INCLUDE_DIR)
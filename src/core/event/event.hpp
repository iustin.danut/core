// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#pragma once

#include <functional>

namespace core::event {
    class Event {
       public:
        Event(unsigned long id, std::function<int()> function, int timeout);
        virtual ~Event();

        [[nodiscard]] unsigned long GetId() { return _id; }

        bool DecrementTimeout(unsigned int elapsed);

        /**
         * Will execute this event and refresh the invocation time.
         */
        bool Execute();

        /**
         * Will cancel this event manually.
         */
        void Cancel();

       private:
        unsigned long _id;
        std::function<int()> _function;
        int _invocationTime;
    };
}  // namespace core::event

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <boost/format.hpp>
#include <memory>
#include <string>

#include "result_set.hpp"

namespace core::mysql {
    class MySQL;
    class MySQLError;

    class Statement {
       public:
        Statement(std::shared_ptr<MySQL> mysql, const std::string &statement);

        std::string EscapeString(const std::string &str);

        friend Statement &operator<<(Statement &stmt, const std::string &value);
        friend Statement &operator<<(Statement &stmt, int value);

        std::shared_ptr<ResultSet> ExecuteSync();
        void Execute(const std::function<void(const MySQLError &, std::shared_ptr<ResultSet>)> &cb);
        void ExecuteMultiStatement(
            const std::function<void(const MySQLError &, std::shared_ptr<std::vector<std::shared_ptr<ResultSet>>>)>
                &cb);

        [[deprecated]] std::shared_ptr<ResultSet> operator()();

        std::string GetStatement();

       private:
        std::string _statement;
        boost::format _formatter;
        std::shared_ptr<MySQL> _mysql;
    };
}  // namespace core::mysql
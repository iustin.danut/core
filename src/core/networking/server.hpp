// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <boost/asio.hpp>
#include <mutex>
#include <string>

#include "../logger.hpp"
#include "connection.hpp"
#include "packet_manager.hpp"

namespace core {
    class Application;

    namespace networking {
        class Server : public std::enable_shared_from_this<Server> {
           public:
            typedef std::function<void(std::shared_ptr<Connection> connection,
                                       std::shared_ptr<Packet> packet)>
                PacketHandler;

            typedef std::function<void(std::shared_ptr<Connection> connection)>
                NewConnectionHandler;
            typedef std::function<void(std::shared_ptr<Connection> connection)>
                DisconnectHandler;

           public:
            Server(std::shared_ptr<Application> application, std::string host,
                   unsigned short port, uint8_t securityLevel,
                   const uint32_t defaultKeys[]);
            virtual ~Server();

           public:
            void Run();

            std::shared_ptr<PacketManager> GetPacketManager();

            void SetNewConnectionHandler(NewConnectionHandler handler);
            void SetDisconnectHandler(DisconnectHandler handler);
            void RegisterHandler(uint8_t header, PacketHandler handler);

            void CallNewConnectionHandler(
                const std::shared_ptr<Connection> &connection);
            void CallDisconnectHandler(
                const std::shared_ptr<Connection> &connection);
            void CallPacketHandler(std::shared_ptr<Connection> connection,
                                   std::shared_ptr<Packet> packet);

            void SendToAll(const std::shared_ptr<Packet> &packet);
            void SendTo(
                const std::shared_ptr<Packet> &packet,
                std::function<bool(std::shared_ptr<Connection> &)> filter);
            void SendToPhase(std::shared_ptr<Packet> packet, uint8_t phase);

            std::shared_ptr<Connection> GetConnection(uint64_t id);
            void RemoveConnection(uint64_t id);

            std::shared_ptr<Application> GetApplication();

           private:
            void RegisterDefault();

            void StartAccept();
            void HandleAccept(std::shared_ptr<Connection> connection,
                              const boost::system::error_code &err);

            void AssignConnection(std::shared_ptr<Connection> connection);

           private:
            std::shared_ptr<Application> _application;
            std::shared_ptr<PacketManager> _packetManager;
            std::map<uint8_t, PacketHandler> _packetHandlers;
            NewConnectionHandler _newConnectionHandler;
            DisconnectHandler _disconnectHandler;

            std::mutex _connectionsMutex;
            uint64_t _nextConnectionId;
            std::map<uint64_t, std::shared_ptr<Connection>> _connections;

            uint8_t _securityLevel;
            uint32_t _defaultKeys[4];

            std::string _host;
            unsigned short _port;
            boost::asio::ip::tcp::acceptor _acceptor;
        };
    }  // namespace networking
}  // namespace core

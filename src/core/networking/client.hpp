// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <boost/asio.hpp>
#include <random>

#include "packet_manager.hpp"

namespace core::networking {
    /// Used to communicate with dbcache
    /// No handshake implemented
    class Client : public std::enable_shared_from_this<Client> {
        typedef std::function<void(std::shared_ptr<Packet>,
                                   std::shared_ptr<Client>)>
            RequestHandler;

       public:
        Client(const std::string &hostname, unsigned short port,
               boost::asio::io_service &service);
        ~Client();

        std::shared_ptr<PacketManager> GetPacketManager() const;

        void Start();

        void SendPacket(std::shared_ptr<Packet> packet);
        void SendPacket(std::shared_ptr<Packet> packet, RequestHandler handler);
        void Close();

       private:
        void ReadNextPacket();

        void HandleReadHeader(const boost::system::error_code &err,
                              std::size_t transferred);
        void HandleReadData(std::shared_ptr<Packet> packet,
                            const boost::system::error_code &err,
                            std::size_t transferred);
        void HandleWrite(std::vector<uint8_t> *buffer,
                         const boost::system::error_code &err,
                         std::size_t transferred);

       private:
        std::mt19937 _random;

        boost::asio::streambuf _buffer;
        boost::asio::ip::tcp::socket _socket;

        std::shared_ptr<PacketManager> _packetManager;

        std::map<unsigned long long, RequestHandler> _outstandingReplies;
    };
}  // namespace core::networking
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <boost/filesystem.hpp>
#include <memory>

namespace core::scripting {
    class ScriptingEngineAbstract;

    class ScriptingContextAbstract {
       public:
        ScriptingContextAbstract(
            std::shared_ptr<ScriptingEngineAbstract> engine);
        virtual ~ScriptingContextAbstract();

        virtual bool Load(boost::filesystem::path file) = 0;
        virtual bool CallEntryPoint() = 0;

       private:
        std::shared_ptr<ScriptingEngineAbstract> _engine;
    };
}  // namespace core::scripting

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "redis.hpp"

#include <utility>

namespace core::cache {
    Redis::Redis(
        std::shared_ptr<bredis::Connection<boost::asio::ip::tcp::socket>>
            connection)
        : _connection(std::move(connection)) {}
}  // namespace core::cache
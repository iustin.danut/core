// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#pragma once

#include <boost/format.hpp>
#include <memory>
#include <string>

#define TR(str, ...) core::LanguageSystem::GetInstance()->Translate(str, "", ##__VA_ARGS__)
#define TRP(player, str, ...) core::LanguageSystem::GetInstance()->Translate(str, player->GetLanguage(), ##__VA_ARGS__)

namespace core {
    class Language {
       public:
        Language(std::string language);
        virtual ~Language();

        bool Translate(const std::string &originalSource, std::string &targetSource);

       private:
        std::string _language;
        std::unordered_map<std::string, std::string> _strings;
    };

    class LanguageSystem {
       public:
        LanguageSystem();
        virtual ~LanguageSystem();

        void Load();

        std::string TranslateString(const std::string &source, const std::string &targetLanguage);

        template <typename... Args>
        std::string Translate(const std::string &source, const std::string &targetLanguage, Args... args) {
            boost::format f(TranslateString(source, targetLanguage));
            return (f % ... % std::forward<Args>(args)).str();
        }

        static std::shared_ptr<LanguageSystem> GetInstance();

       private:
        std::string _fallbackLanguage = "en";
        std::unordered_map<std::string, std::shared_ptr<Language>> _languages;
    };
}  // namespace core
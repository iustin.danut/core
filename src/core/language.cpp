// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "language.hpp"

#include <boost/filesystem.hpp>
#include <pugixml.hpp>
#include <utility>

#include "logger.hpp"

namespace core {
    LanguageSystem::LanguageSystem() = default;

    LanguageSystem::~LanguageSystem() = default;

    void LanguageSystem::Load() {
        boost::filesystem::path localeDir{"locale/"};
        if (!boost::filesystem::is_directory(localeDir)) {
            CORE_LOGGING(error) << "No locale directory found!";
            return;
        }

        for (auto &file : boost::filesystem::directory_iterator(localeDir)) {
            if (boost::filesystem::is_regular_file(file)) {
                auto languageName = file.path().stem().string();
                auto lang = std::make_shared<Language>(languageName);

                _languages.emplace(languageName, std::move(lang));
            }
        }
    }

    std::shared_ptr<LanguageSystem> LanguageSystem::GetInstance() {
        static std::shared_ptr<LanguageSystem> instance;
        if (!instance) {
            instance = std::make_shared<LanguageSystem>();
        }
        return instance;
    }

    std::string LanguageSystem::TranslateString(const std::string &source, const std::string &targetLanguage) {
        std::string translated;

        if (!targetLanguage.empty() && _languages.find(targetLanguage) != _languages.end()) {
            if (_languages[targetLanguage]->Translate(source, translated)) {
                return translated;
            }
        }

        if (_languages.find(_fallbackLanguage) != _languages.end()) {
            if (_languages[_fallbackLanguage]->Translate(source, translated)) {
                return translated;
            }
        }

        return source;
    }

    Language::Language(std::string language) : _language(std::move(language)), _strings() {
        auto path = (boost::filesystem::path("locale/") / (_language + ".xlf")).string();

        pugi::xml_document doc;
        auto result = doc.load_file(path.c_str());
        if (!result) {
            CORE_LOGGING(trace) << path;
            CORE_LOGGING(error) << "Failed to language file " << _language << ": " << result.description();
            return;  // invalid language file
        }

        // todo currently we only support one source file inside xliff file
        auto units = doc.select_nodes("/xliff/file/body/trans-unit");
        for (auto node : units) {
            auto source = node.node().child_value("source");
            auto target = node.node().child("target") ? node.node().child_value("target") : source;
            _strings[std::string(source)] = std::string(target);
            CORE_LOGGING(trace) << _language << ": " << source << " -> " << target;
        }
    }

    Language::~Language() = default;

    bool Language::Translate(const std::string &originalSource, std::string &targetSource) {
        if (_strings.find(originalSource) == _strings.end()) return false;

        targetSource = _strings[originalSource];
        return true;
    }
}  // namespace core
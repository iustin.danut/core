namespace "game::packets";
target "packets/";
core-directory "../../core/";

/// Used for authentication when transitioning between game cores
packet Login(->0x6d) {
    string(31) username;
    uint32 key;
    uint32 xteakeys[4];
    sequence;
}

/// Send player empire to the client for character selection
/// It also receive the empire the client wants to create
/// Sequence: Only affects the data received
packet Empire(<->0x5a) {
    uint8 empire;
	sequence;
}

/// Creates a new character
packet CreateCharacter(->0x04) {
	uint8 slot;
	string(25) name;
	uint16 class;
	uint8 appearance;
	raw(4) unknown;
	sequence;
}

/// Sends an error during character creation
packet CharacterCreationFail(<-0x09) {
	uint8 error;
}

packet CharacterCreationSuccess(<-0x08) {
	type Character {
		uint32 id;
		string(25) name;
		uint8 class;
		uint8 level;
		uint32 playtime;
		uint8 st;
		uint8 ht;
		uint8 dx;
		uint8 iq;
		uint16 bodyPart;
		uint8 nameChange;
		uint16 hairPart;
		uint32 unknown;
		int32 posX;
		int32 posY;
		int32 ip;
		uint16 port;
		uint8 skill;
	}

	uint8 slot;
	Character character;
}

/// A list of all characters and the needed information for character selection
packet Characters(<-0x20) {
	type Character {
		uint32 id;
		string(25) name;
		uint8 class;
		uint8 level;
		uint32 playtime;
		uint8 st;
		uint8 ht;
		uint8 dx;
		uint8 iq;
		uint16 bodyPart;
		uint8 nameChange;
		uint16 hairPart;
		uint32 unknown;
		int32 posX;
		int32 posY;
		int32 ip;
		uint16 port;
		uint8 skill;
	}
	
    Character characters[4];
    uint32 guildIds[4];
    string(13) guildNames[4];
    uint32 unknown1;
    uint32 unknown2;
}

/// Send then the player selects a character
packet SelectCharacterSlot(->0x06) {
    uint8 slot;
    sequence;
}

packet CharacterDetails(<-0x71) {
    uint32 vid;
    uint16 class;
    string(25) name;
    int32 posX;
    int32 posY;
    int32 posZ;
    uint8 empire;
    uint8 skillGroup;
}

/// Send by the client to identify the client executable name and version
packet Version(->0xf1) {
    string(33) name;
    string(33) timestamp;
    sequence;
}

/// Same as 0xF1 but with another ID
packet Version2(->0xfd) {
    string(33) name;
    string(33) timestamp;
    sequence;
}

/// Client wants to enter the game
packet EnterGame(->0x0a) {
    sequence;
}

/// Synchronize the game/server time
packet GameTime(<-0x6a) {
    uint32 time;
}

/// Set the channel id for the client
packet Channel(<-0x79) {
    uint8 channel;
}

/// Spawn a new character
packet AddCharacter(<-0x01) {
    uint32 vid;
    float angle;
    int32 x;
    int32 y;
    int32 z;
    uint8 characterType;
    uint16 class;
    uint8 moveSpeed;
    uint8 attackSpeed;
    uint8 state;
    uint32 affects[2];
}

/// Despawn a character or monster
packet RemoveCharacter(<-0x02) {
    uint32 vid;
}

/// Additional information for a character
packet CharacterInfo(<-0x88) {
    uint32 vid;
    string(25) name;
    uint16 parts[4];
    uint8 empire;
    uint32 guildId;
    uint32 level;
    int16 rankPoints;
    uint8 pkMode;
    uint32 mountVnum;
}

/// Update for a character
packet CharacterUpdate(<-0x13) {
    uint32 vid;
    uint16 parts[4];
    uint8 moveSpeed;
    uint8 attackSpeed;
    uint8 state;
    uint32 affects[2];
    uint32 guildId;
    int16 rankPoints;
    uint8 pkMode;
    uint32 mountVnum;
}

/// Send by the client if the player selects another player / monster or npc
packet TargetChange(->0x3d) {
    uint32 targetVid;
    sequence;
}

/// Send by the server to confirm target
packet SetTarget(<-0x3f) {
    uint32 targetVid;
    uint8 percentage;
}

/// Update all player points
packet CharacterPoints(<-0x10) {
    uint32 points[255];
}

/// Send any chat message
packet ChatOutgoing(<-0x04) {
    uint16 size;
    uint8 messageType;
    uint32 vid;
    uint8 empire;
    dynamic;
}

/// The player send a chat message, this could also be a command like "/exit"
packet ChatIncoming(->0x03) {
    uint16 size;
    uint8 messageType;
    dynamic;
    sequence;
}

packet SetItem(<-0x15) {
    type Bonus {
        uint8 bonusId;
        uint16 value;
    }

    uint8 window;
    uint16 position;
    uint32 vnum;
    uint8 count;
    uint32 flags;
    uint32 antiFlags;
    uint8 highlight;
    uint32 sockets[3];
    Bonus bonuses[7];
}

packet ItemMove(->0x0d) {
    uint8 fromWindow;
    uint16 fromPosition;
    uint8 toWindow;
    uint16 toPosition;
    uint8 count;
    sequence;
}

packet CharacterMove(->0x07) {
    uint8 movementType;
    uint8 argument;
    uint8 rotation;
    int32 x;
    int32 y;
    uint32 time;
    sequence;
}

packet CharacterMoveOut(<-0x03) {
    uint8 movementType;
    uint8 argument;
    uint8 rotation;
    uint32 vid;
    int32 x;
    int32 y;
    uint32 time;
    uint32 duration;
}

packet Attack(->0x02) {
    uint8 attackType;
    uint32 vid;
    uint8 unknown[2];
    sequence;
}

packet DamageInfo(<-0x87) {
    uint32 vid;
    uint8 damageType;
    int32 damage;
}

packet Dead(<-0x0e) {
    uint32 vid;
}

packet Teleport(<-0x41) {
    int32 x;
    int32 y;
    int32 address;
    uint16 port;
}
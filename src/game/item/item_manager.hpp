// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <map>
#include <memory>

#include "../../core/cache/redis.hpp"
#include "../../core/mysql/mysql.hpp"
#include "../formats/item_proto.hpp"

namespace game::item {
    class Item;

    class ItemManager : public std::enable_shared_from_this<ItemManager> {
        friend Item;

       public:
        ItemManager(std::shared_ptr<core::cache::Redis> redis, std::shared_ptr<core::mysql::MySQL> database);
        virtual ~ItemManager();

        void QueryItems(uint32_t playerId, uint16_t window,
                        const std::function<void(std::shared_ptr<std::vector<std::shared_ptr<Item>>>)> &cb);

        void RemoveFromWindow(uint16_t window, uint32_t playerId, uint64_t id);
        void AddToWindow(uint16_t window, uint32_t playerId, uint64_t id);

        std::shared_ptr<Item> CreateItem(uint32_t vnum, uint32_t owner);
        void GetItemById(uint64_t id, const std::function<void(std::shared_ptr<Item>)> &cb);

        void Load();

        bool HasProto(uint32_t id) const;
        const formats::Item &GetProto(uint32_t id) const;

       private:
        std::shared_ptr<core::cache::Redis> _redis;
        std::shared_ptr<core::mysql::MySQL> _database;
        std::map<uint32_t, formats::Item> _protos;
    };
}  // namespace game::item

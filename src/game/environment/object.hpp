// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <memory>
#include <string>
#include <unordered_map>

#include "../../core/networking/packet.hpp"

namespace core::networking {
    class Connection;
}

namespace game::environment {
    class Map;
    class Player;

    enum ObjectType { MONSTER = 0, NPC = 1, PLAYER = 6 };

    class Object {
       public:
        explicit Object(uint32_t vid, int32_t x = 0, int32_t y = 0, float rotation = 0)
            : _vid(vid), _posX(x), _posY(y), _rotation(rotation), _positionModified(true) {}
        virtual ~Object() {}

        virtual void Show(const std::shared_ptr<core::networking::Connection> &connection) = 0;
        virtual void Remove(const std::shared_ptr<core::networking::Connection> &connection) = 0;
        virtual void Update(uint32_t elapsedTime);
        virtual void OnSpawned() = 0;

        virtual void TakeDamage(const std::shared_ptr<Object> &object, int damage) = 0;

        virtual void ObjectEnteredView(std::shared_ptr<Object> object) = 0;
        virtual void ObjectLeftView(std::shared_ptr<Object> object) = 0;
        virtual std::shared_ptr<Object> GetPointer() = 0;

        virtual int32_t GetHP() = 0;
        virtual int32_t GetMaxHP() = 0;

        void AddObjectToView(const std::shared_ptr<Object> object);
        void RemoveObjectFromView(const std::shared_ptr<Object> object);
        void RemoveObjectFromView();
        void SendPacketAround(const std::shared_ptr<core::networking::Packet> &packet);
        void ForEachAround(const std::function<void(const std::shared_ptr<Object> &)> &function);
        size_t GetObjectsInViewCount() { return _viewObjects.size(); }

        void AddTargetedBy(uint32_t vid, const std::weak_ptr<Player> &object);
        void RemoveTargetedBy(uint32_t vid);

        virtual ObjectType GetObjectType() const = 0;
        virtual const std::string &GetName() const = 0;

        [[nodiscard]] uint32_t GetVID() const { return _vid; }

        [[nodiscard]] int32_t GetPositionX() const { return _posX; }
        void SetPositionX(int32_t posX) {
            if (_posX != posX) _positionModified = true;
            _posX = posX;
        }

        [[nodiscard]] int32_t GetPositionY() const { return _posY; }
        void SetPositionY(int32_t posY) {
            if (_posY != posY) _positionModified = true;
            _posY = posY;
        }

        [[nodiscard]] float GetRotation() const { return _rotation; }
        void SetRotation(float rotation) { _rotation = rotation; }

        [[nodiscard]] std::shared_ptr<Map> GetMap() const { return _map; }
        void SetMap(std::shared_ptr<Map> map) { _map = map; }

        [[nodiscard]] bool GetPositionModified() const { return _positionModified; }
        void SetPositionModified(bool positionModified) { _positionModified = positionModified; }

        void RefreshTarget();
        void ClearTarget();

        bool IsDead() { return _dead; }

        virtual void Dead() { _dead = true; }

       protected:
        uint32_t _vid;
        bool _positionModified;
        float _rotation;
        std::shared_ptr<Map> _map;

        bool _dead = false;

       private:
        std::unordered_map<uint32_t, std::weak_ptr<Object>> _viewObjects;
        std::mutex _viewObjectsMutex;

        std::unordered_map<uint32_t, std::weak_ptr<Player>> _targetedBy;
        std::mutex _targetedByMutex;

        int32_t _posX;
        int32_t _posY;
    };
}  // namespace game::environment
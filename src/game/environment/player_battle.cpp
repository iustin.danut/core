// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include <cstdint>

#include "../../core/networking/reserved_headers.hpp"
#include "../application.hpp"
#include "player.hpp"

#define DEBUG_DAMAGE

namespace game::environment {
    uint8_t Player::GetClassStatusBonus() const {
        switch (_playerClass) {
            case 0:
            case 4:
                return GetPoint(POINT_ST);
            case 1:
            case 5:
                return GetPoint(POINT_DX);
            case 2:
            case 6:
                return GetPoint(POINT_ST);
            case 3:
            case 7:
                return GetPoint(POINT_IQ);
            default:
                assert(false);
                return 0;
        }
    }

    uint32_t Player::GetHitRate() const {
        uint32_t base = (GetPoint(POINT_DX) * 4 + GetPoint(POINT_LEVEL) * 2) / 6;
        return 100 * ((base > 90 ? 90 : base) + 210) / 300;
    }

    uint32_t Player::CalculateAttackDamage(uint32_t weaponDamage) const {
        uint32_t levelBonus = GetPoint(POINT_LEVEL) * 2;
        uint32_t statusBonus = (4 * GetPoint(POINT_ST) + 2 * GetClassStatusBonus()) / 3;

        uint32_t calcWeaponDamage = weaponDamage * 2;

        return levelBonus + (statusBonus + calcWeaponDamage) * GetHitRate() / 100;
    }

    uint32_t Player::GetPoint(uint8_t point) const {
        switch (point) {
            case POINT_LEVEL:
                return _level;
            case POINT_HP:
                return _hp;
            case POINT_MAX_HP: {
                const auto &job = Application::GetInstance()->GetJobManager()->GetJob(_playerClass / 2);
                return job.startHp + job.hpPerHt * GetPoint(POINT_HT) +
                       job.hpPerLevel * GetPoint(POINT_LEVEL);  // todo: cache
            }
            case POINT_SP:
                return 0;
            case POINT_MAX_SP:
                return 0;  // todo: cache
            case POINT_ST:
                return _st;
            case POINT_HT:
                return _ht;
            case POINT_DX:
                return _dx;
            case POINT_IQ:
                return _iq;
            case POINT_ATTACK_SPEED:
                return _attackSpeed;
            case POINT_MOVE_SPEED:
                return _moveSpeed;
            case POINT_ATTACK_DAMAGE:
                return 0;
            case POINT_MIN_ATTACK_DAMAGE:
                return CalculateAttackDamage(GetPoint(POINT_MIN_WEAPON_DAMAGE));
            case POINT_MAX_ATTACK_DAMAGE:
                return CalculateAttackDamage(GetPoint(POINT_MAX_WEAPON_DAMAGE));
            case POINT_MIN_WEAPON_DAMAGE:
                if (_weapon) {
                    const auto &proto = _weapon->GetProto();
                    return proto.values[3] + proto.values[5];
                }
                return 0;
            case POINT_MAX_WEAPON_DAMAGE:
                if (_weapon) {
                    const auto &proto = _weapon->GetProto();
                    return proto.values[4] + proto.values[5];
                }
                return 0;
            default:
                return 0;
        }
    }

    void Player::SetPoint(uint8_t point, uint32_t value) {
        switch (point) {
            case POINT_LEVEL:
                _level = value;
                break;
            case POINT_HP:
                _hp = value;
                break;
            case POINT_ST:
                _st = value;
                break;
            case POINT_HT:
                _ht = value;
                break;
            case POINT_DX:
                _dx = value;
                break;
            case POINT_IQ:
                _iq = value;
                break;
            case POINT_ATTACK_SPEED:
                _attackSpeed = value;
                break;
            case POINT_MOVE_SPEED:
                _moveSpeed = value;
                break;
            default:
                assert(false);
        }
    }

    void Player::Attack(const std::shared_ptr<Object> &target) {
        SendChatMessage(CHAT_INFO, "Attacking " + GetName() + " -> " + target->GetName());

        // Calculate attack damage from equipped weapon and status points
        float weaponDamage = Application::GetInstance()->GetCoreApplication()->GetRandomNumber(
            GetPoint(POINT_MIN_ATTACK_DAMAGE), GetPoint(POINT_MAX_ATTACK_DAMAGE));
#ifdef DEBUG_DAMAGE
        SendChatMessage(CHAT_INFO, "Damage: " + std::to_string(weaponDamage));
#endif

        // todo: Calculate raw attack damage
        int damage = static_cast<int>(weaponDamage);
        target->TakeDamage(GetPointer(), damage);
    }

    void Player::TakeDamage(const std::shared_ptr<Object> &object, int damage) {
        SendChatMessage(CHAT_INFO, "Attacking " + GetName() + " <- " + object->GetName());

        // todo: calculate defense
        _hp -= damage;

        auto info = _connection->GetServer()->GetPacketManager()->CreatePacket(0x87);
        info->SetField<uint32_t>("vid", GetVID());
        info->SetField<uint8_t>("damageType", 1);
        info->SetField<int32_t>("damage", damage);
        _connection->Send(info);

        SendPoints();

        if (_hp <= 0) {
            Dead();
        }
    }

    void Player::Dead() {
        if (_dead) return;

        _dead = true;

        auto packet = _connection->GetServer()->GetPacketManager()->CreatePacket(0x0e);
        packet->SetField<uint32_t>("vid", GetVID());
        SendPacketAround(packet);
        _connection->Send(packet);
    }

    void Player::Respawn(bool town) {
        if (!_dead) return;

        _dead = false;

        // todo implement town respawn
        SendChatMessage(CHAT_COMMAND, "CloseRestartWindow");
        _connection->SetPhase(core::networking::Phases::PHASE_GAME);

        Remove(_connection);
        Show(_connection);

        ForEachAround([this](const std::shared_ptr<Object> &object) { object->Show(_connection); });

        // todo set invisible etc
        SetPoint(POINT_HP, GetPoint(POINT_MAX_HP));
        SendPoints();
    }
}  // namespace game::environment
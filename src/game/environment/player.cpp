// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "player.hpp"

#include <utility>

#include "../../core/language.hpp"
#include "../../core/logger.hpp"
#include "../../core/networking/utils.hpp"
#include "../../core/profiler.hpp"
#include "../../core/utils/math.hpp"
#include "../application.hpp"
#include "../commands/command_manager.hpp"

namespace game::environment {
    Player::Player(uint32_t id, uint32_t vid, std::shared_ptr<core::networking::Connection> connection)
        : Object(vid),
          _persistTimer(0),
          _id(id),
          _targetX(0),
          _targetY(0),
          _state(PlayerState::IDLING),
          _connection(std::move(connection)),
          _inventory(5, 9, 2),
          _name(),
          _playerClass(),
          _skillGroup(),
          _playtime(),
          _level(),
          _exp(),
          _gold(),
          _st(),
          _ht(),
          _dx(),
          _iq(),
          _hp(),
          _mp(),
          _stamina() {}

    Player::~Player() {
        Application::GetInstance()->GetCoreApplication()->GetEventSystem()->CancelEvent(_logoutEvent);

        Persist();
        CORE_LOGGING(trace) << "Destroy player " << _name << " (" << _vid << ")";
    }

    void Player::Load(const std::function<void()> &cb) {
        auto core = game::Application::GetInstance();

        core->GetPlayerCache()->GetPlayer(_id, [this, core, cb](auto data) {
            _name = data.name;
            _playerClass = data.playerClass;
            _skillGroup = data.skillGroup;
            _playtime = data.playtime;
            _level = data.level;
            _exp = data.exp;
            _gold = data.gold;
            _st = data.st;
            _ht = data.ht;
            _dx = data.dx;
            _iq = data.iq;
            SetPositionX(data.posX);
            SetPositionY(data.posY);
            _hp = data.hp;
            _mp = data.mp;
            _stamina = data.stamina;

            if (data.teleportationX > 0 || data.teleportationY > 0) {
                SetPositionX(data.teleportationX);
                SetPositionY(data.teleportationY);
            }

            _inventory.Load(_id, 1, core->GetItemManager(), [this, cb, core]() {
                core->GetItemManager()->QueryItems(_id, 2, [this, cb](auto items) {
                    for (const auto &item : *items) {
                        SetItem(item->GetWindow(), item->GetPosition(), item, false);
                    }

                    SetPoint(POINT_HP, GetPoint(POINT_MAX_HP));
                    cb();
                });
            });
        });
    }

    int32_t Player::GetHP() { return 0; }
    int32_t Player::GetMaxHP() { return 0; }

    void Player::SendBasicData() {
        auto character = _connection->GetServer()->GetPacketManager()->CreatePacket(0x71, core::networking::Outgoing);
        character->SetField<uint32_t>("vid", _vid);
        character->SetString("name", _name);
        character->SetField<uint16_t>("class", _playerClass);
        character->SetField<int32_t>("posX", GetPositionX());
        character->SetField<int32_t>("posY", GetPositionY());
        character->SetField<uint8_t>("empire", 3);
        _connection->Send(character);
    }

    void Player::SendPoints() {
        auto points = _connection->GetServer()->GetPacketManager()->CreatePacket(0x10, core::networking::Outgoing);
        for (auto i = 0; i < 255; i++) {
            points->SetRepeatedField<uint32_t>("points", i, GetPoint(i));
        }
        _connection->Send(points);
    }

    void Player::SendInventory() {
        for (auto &item : _inventory.GetItems()) {
            item.second->Send(_connection);
        }

        if (_body) _body->Send(_connection);
        if (_head) _body->Send(_connection);
        if (_shoes) _body->Send(_connection);
        if (_bracelet) _body->Send(_connection);
        if (_weapon) _weapon->Send(_connection);
        if (_necklace) _necklace->Send(_connection);
        if (_earrings) _earrings->Send(_connection);
    }

    void Player::SendCharacter(const std::shared_ptr<core::networking::Connection> &connection) {
        if (_dead) return;

        auto character = connection->GetServer()->GetPacketManager()->CreatePacket(0x01, core::networking::Outgoing);
        character->SetField<uint32_t>("vid", _vid);
        character->SetField<uint8_t>("characterType", GetObjectType());
        character->SetField<float>("angle", GetRotation());
        character->SetField<int32_t>("x", GetPositionX());
        character->SetField<int32_t>("y", GetPositionY());
        character->SetField<uint16_t>("class", _playerClass);
        character->SetField<uint8_t>("moveSpeed", _moveSpeed);
        character->SetField<uint8_t>("attackSpeed", _attackSpeed);
        connection->Send(character);
    }

    void Player::SendCharacterAdditional(const std::shared_ptr<core::networking::Connection> &connection) {
        if (_dead) return;

        CORE_LOGGING(trace) << "Send additional for " << _name << " (" << _vid << ")";

        auto additional = connection->GetServer()->GetPacketManager()->CreatePacket(0x88, core::networking::Outgoing);
        additional->SetField<uint32_t>("vid", _vid);
        additional->SetString("name", _name);
        additional->SetField<uint8_t>("empire", 3);
        additional->SetField<uint32_t>("level", _level);
        additional->SetRepeatedField<uint16_t>("parts", 0, _body ? _body->GetProto().id : 0);
        additional->SetRepeatedField<uint16_t>("parts", 1, _weapon ? _weapon->GetProto().id : 0);
        additional->SetRepeatedField<uint16_t>("parts", 2, 0);
        additional->SetRepeatedField<uint16_t>("parts", 3, 0);
        connection->Send(additional);
    }

    void Player::SendCharacterUpdate(const std::shared_ptr<core::networking::Connection> &connection) {
        if (_dead) return;

        auto update = connection->GetServer()->GetPacketManager()->CreatePacket(0x13, core::networking::Outgoing);
        update->SetField<uint32_t>("vid", _vid);
        update->SetRepeatedField<uint16_t>("parts", 0, _body ? _body->GetProto().id : 0);
        update->SetRepeatedField<uint16_t>("parts", 1, _weapon ? _weapon->GetProto().id : 0);
        update->SetRepeatedField<uint16_t>("parts", 2, 0);
        update->SetRepeatedField<uint16_t>("parts", 3, 0);
        update->SetField<uint8_t>("moveSpeed", _moveSpeed);
        update->SetField<uint8_t>("attackSpeed", _attackSpeed);
        connection->Send(update);
    }

    void Player::Show(const std::shared_ptr<core::networking::Connection> &connection) {
        if (_dead) return;

        SendCharacter(connection);
        SendCharacterAdditional(connection);
    }

    void Player::Remove(const std::shared_ptr<core::networking::Connection> &connection) {
        auto packet =
            connection->GetServer()->GetPacketManager()->CreatePacket(0x02, core::networking::Direction::Outgoing);
        packet->SetField("vid", GetVID());
        connection->Send(packet);
    }

    std::shared_ptr<item::Item> Player::GetItem(uint8_t window, uint16_t position) {
        switch (window) {
            case 1:
                return _inventory.Get(position);
            case 2:
                switch (position) {
                    case 0:
                        return _body;
                    case 1:
                        return _head;
                    case 2:
                        return _shoes;
                    case 3:
                        return _bracelet;
                    case 4:
                        return _weapon;
                    case 5:
                        return _necklace;
                    case 6:
                        return _earrings;
                    default:
                        return nullptr;
                }
            default:
                return nullptr;
        }
    }

    bool Player::HasSpace(uint8_t window, uint16_t position, uint8_t size) {
        switch (window) {
            case 1:
                return _inventory.HasSpace(position, size);
            case 2:
                switch (position) {
                    case 0:
                        return _body == nullptr;
                    case 1:
                        return _head == nullptr;
                    case 2:
                        return _shoes == nullptr;
                    case 3:
                        return _bracelet == nullptr;
                    case 4:
                        return _weapon == nullptr;
                    case 5:
                        return _necklace == nullptr;
                    case 6:
                        return _earrings == nullptr;
                    default:
                        return false;
                }
            default:
                CORE_LOGGING(error) << "Player::HasSpace Unknown window " << static_cast<uint16_t>(window);
                return false;
        }
    }

    bool Player::RemoveItem(const std::shared_ptr<item::Item> &item, bool send) {
        if (!item) return false;
        auto window = item->GetWindow();
        auto position = item->GetPosition();

        switch (window) {
            case 1:
                _inventory.Clear(position);
                item->SendRemove(_connection);
                item->SetWindow(0);
                item->SetPosition(0);
                return true;
            case 2:
                switch (position) {
                    case 0:
                        _body = nullptr;
                        break;
                    case 1:
                        _head = nullptr;
                        break;
                    case 2:
                        _shoes = nullptr;
                        break;
                    case 3:
                        _bracelet = nullptr;
                        break;
                    case 4:
                        _weapon = nullptr;
                        break;
                    case 5:
                        _necklace = nullptr;
                        break;
                    case 6:
                        _earrings = nullptr;
                        break;
                    default:
                        return false;
                }

                item->SendRemove(_connection);
                item->SetWindow(0);
                item->SetPosition(0);
                SendCharacterUpdate(_connection);  // todo send to view
                SendPoints();
                return true;
            default:
                return false;
        }
    }

    bool Player::SetItem(uint8_t window, uint16_t position, const std::shared_ptr<item::Item> &item, bool send) {
        if (!item) return false;

        switch (window) {
            case 1:
                _inventory.Set(position, item);
                item->SetWindow(window);
                item->SetPosition(position);
                if (send) item->Send(_connection);
                return true;
            case 2:
                switch (position) {
                    case 0:
                        _body = item;
                        break;
                    case 1:
                        _head = item;
                        break;
                    case 2:
                        _shoes = item;
                        break;
                    case 3:
                        _bracelet = item;
                        break;
                    case 4:
                        _weapon = item;
                        break;
                    case 5:
                        _necklace = item;
                        break;
                    case 6:
                        _earrings = item;
                        break;
                    default:
                        return false;
                }

                item->SetWindow(window);
                item->SetPosition(position);
                if (send) item->Send(_connection);
                SendCharacterUpdate(_connection);  // todo send to view
                SendPoints();
                return true;
            default:
                return false;
        }
    }

    bool Player::GiveItem(const std::shared_ptr<item::Item> &item) {
        auto size = item->GetProto().size;
        for (auto i = 0; i < 90; i++) {
            if (HasSpace(1, i, size)) {
                SetItem(1, i, item);
                item->Persist();
                game::Application::GetInstance()->GetItemManager()->AddToWindow(1, _id, item->GetId());
                return true;
            }
        }
        return false;
    }

    bool Player::HasSpace(uint8_t size) {
        for (auto i = 0; i < 90; i++) {
            if (HasSpace(1, i, size)) {
                return true;
            }
        }
        return false;
    }

    void Player::SendChatMessage(uint8_t messageType, const std::string &message) {
        auto packet =
            _connection->GetServer()->GetPacketManager()->CreatePacket(0x04, core::networking::Direction::Outgoing);
        packet->SetField<uint8_t>("messageType", messageType);
        packet->SetDynamicString(message);
        _connection->Send(packet);
    }

    void Player::Move(int32_t x, int32_t y) {
        SetPositionX(x);
        SetPositionY(y);
    }

    void Player::Goto(int32_t x, int32_t y) {
        if (GetPositionX() == x && GetPositionY() == y) return;  // we are already at our target
        if (_targetX == x && _targetY == y) return;              // we already have this position as our target

        // Get animation data
        auto animation = game::Application::GetInstance()->GetAnimationManager()->GetAnimation(
            _playerClass, formats::AnimationType::RUN, formats::AnimationSubType::GENERAL);
        if (!animation) {
            CORE_LOGGING(error) << "Failed to find animation run for " << _playerClass;
            return;
        }

        _state = PlayerState::MOVING;
        _targetX = x;
        _targetY = y;
        _startX = GetPositionX();
        _startY = GetPositionY();
        _movementStart = _connection->GetServer()->GetApplication()->GetCoreTime();

        float distance = core::utils::Math::Distance(_startX, _startY, _targetX, _targetY);
        float animationSpeed = -animation->accumulationY / animation->motionDuration;

        int i = 100 - _moveSpeed;
        if (i > 0) {
            i = 100 + i;
        } else if (i < 0) {
            i = 10000 / (100 - i);
        } else {
            i = 100;
        }
        int duration = static_cast<int>((distance / animationSpeed) * 1000) * i / 100;
        _movementDuration = duration;

        CORE_LOGGING(trace) << "Movement duration: " << _movementDuration;
    }

    void Player::Teleport(int32_t x, int32_t y) {
        auto map = GetMap();
        if (x >= map->GetX() && y >= map->GetY() && x < map->GetX() + map->GetWidth() * UNIT_SIZE &&
            y < map->GetY() + map->GetHeight() * UNIT_SIZE) {
            SetPositionX(x);
            SetPositionY(y);
            _state = PlayerState::IDLING;
            Show(_connection);
            return;
        }

        auto cache = game::Application::GetInstance()->GetPlayerCache();
        cache->SetTeleportationPosition(_id, x, y);

        // Calculate target server
        auto config = game::Application::GetInstance()->GetCoreApplication()->GetConfiguration();
        std::string localAddress;
        if (config.count("ip") != 0) {
            localAddress = config.get<std::string>("ip");
        } else {
            localAddress = core::networking::GetLocalAddress();
        }
        auto localIp = core::networking::ConvertIP(localAddress);

        auto p = _connection->GetServer()->GetPacketManager()->CreatePacket(0x41);
        p->SetField<int32_t>("x", x);
        p->SetField<int32_t>("y", y);
        p->SetField<int32_t>("address", localIp);
        p->SetField<uint16_t>("port", config.get<uint16_t>("port"));
        _connection->Send(p);
    }

    void Player::Update(uint32_t elapsedTime) {
        PROFILE_FUNCTION();

        if (!_map) return;  // We aren't fully spawned yet!

        // Increment persist timer and persist the player every second
        _persistTimer += elapsedTime;
        if (_persistTimer >= 1000) {
            Persist();
            _persistTimer -= 1000;
        }

        auto time = Application::GetInstance()->GetCoreApplication()->GetCoreTime();

        if (_state == PlayerState::MOVING) {
            auto elapsed = time - _movementStart;
            auto rate = elapsed / (float)_movementDuration;
            if (rate > 1) rate = 1;

            auto x = (int32_t)((float)(_targetX - _startX) * rate + _startX);
            auto y = (int32_t)((float)(_targetY - _startY) * rate + _startY);

            Move(x, y);

            if (rate >= 1) {
                _state = PlayerState::IDLING;
                CORE_LOGGING(trace) << "Movement of " << _name << " done";
            }
        }

        Object::Update(elapsedTime);
    }

    void Player::OnSpawned() {}

    void Player::ObjectEnteredView(std::shared_ptr<Object> object) {
        if (_connection) object->Show(_connection);
    }

    void Player::ObjectLeftView(std::shared_ptr<Object> object) {
        if (_connection) object->Remove(_connection);
    }

    void Player::Persist() {
        PROFILE_FUNCTION();

        auto core = game::Application::GetInstance();
        core->GetPlayerCache()->SavePlayer({_id,
                                            _name,
                                            _playerClass,
                                            _skillGroup,
                                            _playtime,
                                            _level,
                                            _exp,
                                            _gold,
                                            _st,
                                            _ht,
                                            _dx,
                                            _iq,
                                            GetPositionX(),
                                            GetPositionY(),
                                            0,
                                            0,
                                            _hp,
                                            _mp,
                                            _stamina});
    }

    void Player::DebugView() {
        ForEachAround([this](auto obj) {
            auto distance =
                core::utils::Math::Distance(obj->GetPositionX(), obj->GetPositionY(), GetPositionX(), GetPositionY());
            SendChatMessage(1,
                            "[" + std::to_string(obj->GetVID()) + "]" + obj->GetName() +
                                " - Distance: " + std::to_string(distance));
        });

        SendChatMessage(1, "-------------------------");
        SendChatMessage(1, "Count: " + std::to_string(GetObjectsInViewCount()));
    }

    void Player::Logout(bool close) {
        _logoutTimer = 10;
        auto evtSystem = Application::GetInstance()->GetCoreApplication()->GetEventSystem();

        evtSystem->CancelEvent(_logoutEvent);
        _logoutEvent = evtSystem->EnqueueEvent(
            [this, close]() {
                if (_logoutTimer == 0) {
                    if (close) SendChatMessage(game::environment::CHAT_COMMAND, "quit");
                    GetConnection()->PostShutdown();

                    return 0;  // will cancel this event
                }

                SendChatMessage(game::environment::CHAT_INFO,
                                TR("You will be logged out in %d seconds.", _logoutTimer));

                _logoutTimer--;
                return 1000;
            },
            0);
    }
}  // namespace game::environment
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <string>
#include <vector>

namespace game::formats {
    class CryptedFile {
       public:
        explicit CryptedFile(const std::vector<uint8_t> &data,
                             const uint32_t *key, unsigned int offset = 0);
        explicit CryptedFile(const std::string &file, const uint32_t *key,
                             unsigned int offset = 0);
        virtual ~CryptedFile();

        const std::vector<uint8_t> &GetData();

       private:
        std::vector<uint8_t> _data;

        std::string _fourCC;
        unsigned int _size;
        unsigned int _compressSize;
        unsigned int _realSize;
    };
}  // namespace game::formats

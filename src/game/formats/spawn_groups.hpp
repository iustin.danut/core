// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#pragma once

#include <cstdint>
#include <string>
#include <unordered_map>
#include <vector>

namespace game::formats {
    struct SpawnGroup {
        uint64_t id;
        std::string name;
        std::vector<uint32_t> members;
    };

    class SpawnGroups {
       public:
        SpawnGroups();
        virtual ~SpawnGroups();

        bool Load(const std::string &path);

        const SpawnGroup &GetGroup(uint64_t id) { return _groups[id]; }

       private:
        std::unordered_map<uint64_t, SpawnGroup> _groups;
    };
}  // namespace game::formats

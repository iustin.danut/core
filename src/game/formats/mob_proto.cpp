// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "mob_proto.hpp"

#include "../../core/logger.hpp"
#include "../../core/utils/file.hpp"
#include "crypted_file.hpp"

namespace game::formats {
    bool MobProto::Load(const std::string &file, std::map<uint32_t, Monster> &monsters) {
        auto data = core::utils::ReadFile(file);

        if (data.empty()) {
            CORE_LOGGING(error) << "Failed to load " << file;
            return false;
        }

        const char *fourCC = reinterpret_cast<const char *>(&data[0]);
        uint32_t elements = *reinterpret_cast<uint32_t *>(&data[4]);
        uint32_t size = *reinterpret_cast<uint32_t *>(&data[8]);

        // Check fourCC
        if (strncmp(fourCC, "MMPT", 4) != 0) {
            CORE_LOGGING(error) << "Failed to load " << file << " invalid header";
            return false;
        }

        CORE_LOGGING(trace) << "Elements: " << elements;

        const uint32_t key[] = {0x497446, 0x4A0B, 0x86EB7, 0x68189D};

        // Decrypt data
        CryptedFile proto(data, &key[0], 12);
        auto decrypted = proto.GetData();

        // Verify mob_proto version
        uint32_t stride = decrypted.size() / elements;
        assert(stride == 255);
        assert(decrypted.size() % elements == 0);

        for (uint32_t i = 0; i < elements; i++) {
            Monster monster;
            monster.Read(&decrypted[i * stride]);
            monsters.emplace(monster.id, std::move(monster));
        }

        return true;
    }

    void Monster::Read(uint8_t *ptr) {
        id = *reinterpret_cast<uint32_t *>(ptr);
        name = std::string(reinterpret_cast<const char *>(ptr + 4), 25);
        name.erase(std::find(name.begin(), name.end(), '\0'), name.end());
        translatedName = std::string(reinterpret_cast<const char *>(ptr + 29), 25);
        translatedName.erase(std::find(translatedName.begin(), translatedName.end(), '\0'), translatedName.end());
        type = *(ptr + 54);
        rank = *(ptr + 55);
        battleType = *(ptr + 56);
        level = *(ptr + 57);
        size = *(ptr + 58);
        minGold = *reinterpret_cast<uint32_t *>(ptr + 59);
        maxGold = *reinterpret_cast<uint32_t *>(ptr + 63);
        experience = *reinterpret_cast<uint32_t *>(ptr + 67);
        hp = *reinterpret_cast<uint32_t *>(ptr + 71);
        regenDelay = *(ptr + 75);
        regenPercentage = *(ptr + 76);
        defence = *reinterpret_cast<uint16_t *>(ptr + 77);
        aiFlag = *reinterpret_cast<uint32_t *>(ptr + 79);
        raceFlag = *reinterpret_cast<uint32_t *>(ptr + 83);
        immuneFlag = *reinterpret_cast<uint32_t *>(ptr + 87);
        st = *(ptr + 91);
        dx = *(ptr + 92);
        ht = *(ptr + 93);
        iq = *(ptr + 94);
        for (uint8_t i = 0; i < 2; i++) {
            damageRange[i] = *reinterpret_cast<uint32_t *>(ptr + 95 + i * 4);
        }
        attackSpeed = *reinterpret_cast<int16_t *>(ptr + 103);
        moveSpeed = *reinterpret_cast<int16_t *>(ptr + 105);
        aggressivePct = *(ptr + 107);
        aggressiveSight = *reinterpret_cast<uint16_t *>(ptr + 108);
        attackRange = *reinterpret_cast<uint16_t *>(ptr + 110);
        for (uint8_t i = 0; i < 6; i++) {
            enchantments[i] = *(ptr + 112 + i);
        }
        for (uint8_t i = 0; i < 11; i++) {
            resists[i] = *(ptr + 118 + i);
        }
        resurrectionId = *reinterpret_cast<uint32_t *>(ptr + 129);
        dropItemId = *reinterpret_cast<uint32_t *>(ptr + 133);
        mountCapacity = *(ptr + 137);
        onClickType = *(ptr + 138);
        empire = *(ptr + 139);
        folder = std::string(reinterpret_cast<const char *>(ptr + 140), 65);
        folder.erase(std::find(folder.begin(), folder.end(), '\0'), folder.end());
        damageMultiply = *reinterpret_cast<float *>(ptr + 205);
        summonId = *reinterpret_cast<uint32_t *>(ptr + 209);
        drainSP = *reinterpret_cast<uint32_t *>(ptr + 213);
        monsterColor = *reinterpret_cast<uint32_t *>(ptr + 217);
        polymorphItemId = *reinterpret_cast<uint32_t *>(ptr + 221);
        for (uint8_t i = 0; i < 5; i++) {
            skills[i].id = *reinterpret_cast<uint32_t *>(ptr + 225 + i * 5);
            skills[i].level = *(ptr + 225 + i * 5 + 4);
        }
        berserkPoint = *(ptr + 250);
        stoneSkinPoint = *(ptr + 251);
        godSpeedPoint = *(ptr + 252);
        deathBlowPoint = *(ptr + 253);
        revivePoint = *(ptr + 254);
    }
}  // namespace game::formats
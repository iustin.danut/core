// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

namespace auth {
    enum HeaderIncomingPackets : uint8_t {
        HEADER_INC_LOGIN = 0x6f,
    };

    enum HeaderOutgoingPackets : uint8_t {
        HEADER_OUT_LOGIN_FAILED = 0x07,
        HEADER_OUT_LOGIN_SUCCESS = 0x96
    };
}  // namespace auth
